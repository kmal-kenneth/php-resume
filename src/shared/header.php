<?php
require_once __DIR__ . '/sessions.php'; ?>
<!DOCTYPE html>
<html>

<head>
    <title><?= $title ?? 'Page' ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="../css/style.css">
</head>

<body>
    <div class="container page" id="app">
        <div class="box box--nav">

            <nav class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-brand">
                    <a class="navbar-item" href="/">
                        <span class="file-icon">
                            <i class="fas fa-file"></i>
                        </span>

                        <p class="title is-size-6 is-link">PHP Resume</p>
                    </a>

                    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false"
                        data-target="navbarBasicExample">
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div id="navbarBasicExample" class="navbar-menu">
                    <div class="navbar-start">

                        <?php
                        $menu = array();

                        if (isset($_SESSION['user_id'])) {
                            $menu = [
                                ['name' => 'Wizard', 'url' => '/wizard'],
                                ['name' => 'Template', 'url' => '/template']
                            ];
                        }

                        foreach ($menu as $link) {
                            if (array_key_exists('sub_menus', $link)) { ?>
                        <div class="navbar-item has-dropdown is-hoverable">
                            <a class="navbar-link">
                                <?= $link['name'] ?>
                            </a>

                            <div class="navbar-dropdown">
                                <?php foreach (
                                    $link['sub_menus']
                                    as $sub_menu
                                ) {
                                    echo "<a class='navbar-item' href='" .
                                        $sub_menu['url'] .
                                        "'>" .
                                        $sub_menu['name'] .
                                        "</a>";
                                } ?>
                            </div>
                        </div>
                        <?php } else {echo "<a class='navbar-item' href='" .
                                    $link['url'] .
                                    "'>" .
                                    $link['name'] .
                                    "</a>";}
                        }
                        ?>
                    </div>
                    <div class="navbar-end">
                        <?php if (isset($_SESSION['user_id'])) { ?>
                        <div class="navbar-item">
                            <h3 class="label">
                                {{user.email}}
                            </h3>
                        </div>

                        <div class="navbar-item">
                            <div class="buttons">
                                <a class="button is-light is-small is-rounded is-info" @click="logOut">
                                    Log out
                                </a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </nav>
        </div>