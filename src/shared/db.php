<?php

require_once __DIR__ . '/config.php';
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../models/Experience.php';
require_once __DIR__ . '/../models/Education.php';
require_once __DIR__ . '/../models/Project.php';
require_once __DIR__ . '/../models/Skill.php';
require_once __DIR__ . '/../models/Lenguage.php';
require_once __DIR__ . '/../models/Hobby.php';
require_once __DIR__ . '/../models/Contribution.php';

use crojasaragonez\UtnDb\PgConnection;

$config_db = new Config();

$con = new PgConnection(
    $config_db->user_db,
    $config_db->password_db,
    $config_db->database_db,
    $config_db->port_db,
    $config_db->addres_db
);
$con->connect();

$userModel = new User($con);
$experienceModel = new Experience($con);
$educationModel = new Education($con);
$projectModel = new Project($con);
$skillModel = new Skill($con);
$lenguageModel = new Lenguage($con);
$hobbyModel = new Hobby($con);
$contributionModel = new Contribution($con);

/*
$sql = "INSERT INTO users(email, password) VALUES ($1, md5($2))";
$con->runStatement($sql, ['estudiante@est.utn.ac.cr', '12345']);

$results = $con->runQuery('SELECT * FROM users WHERE id >= $1', [1]);

var_dump($results);

$con->disconnect();
*/
