<?php

class Lenguage
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM lenguage WHERE user_id = $1',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert($user_id, $lenguage, $porsentage)
    {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.lenguage(
                "user_id",
                lenguage,
                porsentage
                ) VALUES (
                $1,$2,$3)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$lenguage" == "null" ? null : $lenguage /*NOT NULL*/,
                    "$porsentage" == "null" ? null : $porsentage
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update($id, $user_id, $lenguage, $porsentage)
    {
        try {
            $this->connection->runStatement(
                'UPDATE public.lenguage
            SET "user_id"=$2, 
            skill=$3,
            porsentage=$4
            WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$lenguage" == "null" ? null : $lenguage /*NOT NULL*/,
                    "$porsentage" == "null" ? null : $porsentage
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.lenguage WHERE id=$1',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
