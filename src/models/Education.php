<?php

class Education
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM public.education WHERE user_id = $1 ORDER BY finish_year DESC,
                start_year DESC;',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert(
        $user_id,
        $start_year,
        $start_month,
        $finish_year,
        $finish_month,
        $degree,
        $institution_name,
        $institution_web,
        $degree_description
    ) {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.education(
                user_id,
                start_year,
                start_month,
                finish_year,
                finish_month,
                degree,
                institution_name,
                institution_web,
                degree_description
                ) VALUES (
                    $1,$2,$3,$4,$5,$6,$7,$8,$9)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$start_year" == "null" ? null : $start_year /*NOT NULL*/,
                    "$start_month" == "null" ? null : $start_month,
                    "$finish_year" == "null" ? null : $finish_year /*NOT NULL*/,
                    "$finish_month" == "null" ? null : $finish_month,
                    "$degree" == "null" ? null : $degree /*NOT NULL*/,
                    "$institution_name" == "null" ? null : $institution_name,
                    "$institution_web" == "null" ? null : $institution_web,
                    "$degree_description" == "null" ? null : $degree_description
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update(
        $id,
        $user_id,
        $start_year,
        $start_month,
        $finish_year,
        $finish_month,
        $degree,
        $institution_name,
        $institution_web,
        $degree_description
    ) {
        try {
            $this->connection->runStatement(
                'UPDATE public.education 
            SET user_id=$2, 
            start_year=$3, 
            start_month=$4, 
            finish_year=$5, 
            finish_month=$6, 
            "degree"=$7, 
            institution_name=$8, 
            institution_web=$9, 
            degree_description=$10
            WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$start_year" == "null" ? null : $start_year,
                    "$start_month" == "null" ? null : $start_month,
                    "$finish_year" == "null" ? null : $finish_year,
                    "$finish_month" == "null" ? null : $finish_month,
                    "$degree" == "null" ? null : $degree,
                    "$institution_name" == "null" ? null : $institution_name,
                    "$institution_web" == "null" ? null : $institution_web,
                    "$degree_description" == "null" ? null : $degree_description
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.education 
        WHERE id=$1',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
