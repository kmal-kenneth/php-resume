<?php

class Contribution
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM contribution WHERE user_id = $1',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert($user_id, $name, $web, $description)
    {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.contribution(
                    user_id,
                    "name",
                    web,
                    "description"
                    ) VALUES (
                    $1,$2,$3,$4)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$name" == "null" ? null : $name /*NOT NULL*/,
                    "$web" == "null" ? null : $web,
                    "$description" == "null" ? null : $description
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update($id, $user_id, $name, $web, $description)
    {
        try {
            $this->connection->runStatement(
                'UPDATE public.contribution
                SET "user_id"=$2, 
                "name"=$3,
                web=$4,
                "description"=$5
                WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$name" == "null" ? null : $name /*NOT NULL*/,
                    "$web" == "null" ? null : $web,
                    "$description" == "null" ? null : $description
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.contribution WHERE id=$1',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
