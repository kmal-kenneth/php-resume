<?php

class Skill
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM skill WHERE user_id = $1',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert($user_id, $skill, $porsentage)
    {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.skill(
                    "user_id",
                    skill,
                    porsentage
                    ) VALUES (
                    $1,$2,$3)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$skill" == "null" ? null : $skill /*NOT NULL*/,
                    "$porsentage" == "null" ? null : $porsentage
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update($id, $user_id, $skill, $porsentage)
    {
        try {
            $this->connection->runStatement(
                'UPDATE public.skill
                SET "user_id"=$2, 
                skill=$3,
                porsentage=$4
                WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$skill" == "null" ? null : $skill /*NOT NULL*/,
                    "$porsentage" == "null" ? null : $porsentage
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.skill WHERE id=$1;',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
