<?php

class User
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function login($email, $password)
    {
        try {
            return $this->connection->runQuery(
                "SELECT * FROM public.user WHERE email = $1 and password = $2",
                [$email, md5($password)]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function find($id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM public.user WHERE id = $1',
                [$id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function create($email, $password)
    {
        try {
            return $this->connection->runStatement(
                'INSERT INTO public.user(email, password)  VALUES ($1,$2)',
                [$email, md5($password)]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update(
        $id,
        $email,
        $first_login,
        $first_name,
        $middle_name,
        $last_name,
        $profession,
        $photo_link,
        $profile,
        $phone_number,
        $web,
        $git_repository,
        $address,
        $city,
        $country,
        $knowledge,
        $year_of_birth,
        $place_of_birth,
        $citizen_id,
        $driver_license
    ) {
        try {
            $this->connection->runStatement(
                'UPDATE public."user"
            SET  email=$2, first_login=$3, first_name=$4, middle_name=$5, 
                last_name=$6, profession=$7, photo_link=$8, profile=$9, 
                phone_number=$10, web=$11, git_repository=$12, address=$13, 
                city=$14, country=$15, knowledge=$16, year_of_birth=$17, 
                place_of_birth=$18, citizen_id=$19, driver_license=$20
            WHERE id=$1;',
                [
                    $id,
                    "$email" == "null" ? null : $email,
                    "$first_login" == "null" ? null : $first_login,
                    "$first_name" == "null" ? null : $first_name,
                    "$middle_name" == "null" ? null : $middle_name,
                    "$last_name" == "null" ? null : $last_name,
                    "$profession" == "null" ? null : $profession,
                    "$photo_link" == "null" ? null : $photo_link,
                    "$profile" == "null" ? null : $profile,
                    "$phone_number" == "null" ? null : $phone_number,
                    "$web" == "null" ? null : $web,
                    "$git_repository" == "null" ? null : $git_repository,
                    "$address" == "null" ? null : $address,
                    "$city" == "null" ? null : $city,
                    "$country" == "null" ? null : $country,
                    "$knowledge" == "null" ? null : $knowledge,
                    "$year_of_birth" == "null" ? null : $year_of_birth,
                    "$place_of_birth" == "null" ? null : $place_of_birth,
                    "$citizen_id" == "null" ? null : $citizen_id,
                    "$driver_license" == "null" ? null : $driver_license
                ]
            );

            return true;
        } catch (exception $e) {
            return false;
        }
    }

    /**public function delete($id)
    {
        $this->connection->runStatement('DELETE FROM animals
    WHERE id=$1', [$id]);
    }**/
}
