<?php

class Experience
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT id, user_id, start_year, start_month, finish_year, finish_month, "position", company_name, company_web, job_description
      FROM public.experience WHERE user_id= $1 ORDER BY finish_year DESC,
       start_year DESC;',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert(
        $user_id,
        $start_year,
        $start_month,
        $finish_year,
        $finish_month,
        $position,
        $company_name,
        $company_web,
        $job_description
    ) {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.experience(
                user_id, start_year, start_month, finish_year, finish_month, "position", company_name, company_web, job_description)
                VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9);',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$start_year" == "null" ? null : $start_year /*NOT NULL*/,
                    "$start_month" == "null" ? null : $start_month,
                    "$finish_year" == "null" ? null : $finish_year /*NOT NULL*/,
                    "$finish_month" == "null" ? null : $finish_month,
                    "$position" == "null" ? null : $position,
                    "$company_name" == "null" ? null : $company_name,
                    "$company_web" == "null" ? null : $company_web,
                    "$job_description" == "null"
                        ? null
                        : $job_description /*NOT NULL*/
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update(
        $id,
        $user_id,
        $start_year,
        $start_month,
        $finish_year,
        $finish_month,
        $position,
        $company_name,
        $company_web,
        $job_description
    ) {
        try {
            $this->connection->runStatement(
                'UPDATE public.experience
      SET user_id=$2, start_year=$3, start_month=$4, finish_year=$5, finish_month=$6, "position"=$7, company_name=$8, company_web=$9, job_description=$10
      WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$start_year" == "null" ? null : $start_year,
                    "$start_month" == "null" ? null : $start_month,
                    "$finish_year" == "null" ? null : $finish_year,
                    "$finish_month" == "null" ? null : $finish_month,
                    "$position" == "null" ? null : $position,
                    "$company_name" == "null" ? null : $company_name,
                    "$company_web" == "null" ? null : $company_web,
                    "$job_description" == "null" ? null : $job_description
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.experience WHERE id=$1;',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
