<?php

class Project
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM public.project WHERE user_id = $1',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert($user_id, $name, $web, $description, $platform)
    {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.project(
                user_id,
                "name",
                web,
                "description",
                platform
                ) VALUES (
                $1,$2,$3,$4,$5)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$name" == "null" ? null : $name /*NOT NULL*/,
                    "$web" == "null" ? null : $web,
                    "$description" == "null" ? null : $description,
                    "$platform" == "null" ? null : $platform
                ]
            );

            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update($id, $user_id, $name, $web, $description, $platform)
    {
        try {
            $this->connection->runStatement(
                'UPDATE public.project 
            SET "user_id"=$2, 
            "name"=$3,
            web=$4,
            "description"=$5,
            platform=$6
            WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$name" == "null" ? null : $name /*NOT NULL*/,
                    "$web" == "null" ? null : $web,
                    "$description" == "null" ? null : $description,
                    "$platform" == "null" ? null : $platform
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.project 
        WHERE id=$1',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
