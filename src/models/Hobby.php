<?php

class Hobby
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAll($user_id)
    {
        try {
            return $this->connection->runQuery(
                'SELECT * FROM hobby WHERE user_id = $1',
                [$user_id]
            );
        } catch (exception $e) {
            return false;
        }
    }

    public function insert($user_id, $hobby, $icon, $web)
    {
        try {
            $this->connection->runStatement(
                'INSERT INTO public.hobby(
                    "user_id",
                    hobby,
                    icon,
                    web
                    ) VALUES (
                    $1,$2,$3,$4)',
                [
                    "$user_id" == "null" ? null : $user_id,
                    "$hobby" == "null" ? null : $hobby /*NOT NULL*/,
                    "$icon" == "null" ? null : $icon /*NOT NULL*/,
                    "$web" == "null" ? null : $web
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function update($id, $user_id, $hobby, $icon, $web)
    {
        try {
            $this->connection->runStatement(
                'UPDATE public.hobby
                SET "user_id"=$2, 
                hobby=$3,
                icon=$4,
                web=$5
                WHERE id=$1;',
                [
                    $id,
                    "$user_id" == "null" ? null : $user_id,
                    "$hobby" == "null" ? null : $hobby /*NOT NULL*/,
                    "$icon" == "null" ? null : $icon /*NOT NULL*/,
                    "$web" == "null" ? null : $web
                ]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }

    public function delete($id)
    {
        try {
            $this->connection->runStatement(
                'DELETE FROM public.lenguage WHERE id=$1',
                [$id]
            );
            return true;
        } catch (exception $e) {
            return false;
        }
    }
}
