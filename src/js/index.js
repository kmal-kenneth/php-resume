var app = new Vue({
  el: "#app",
  data: {
    months: new Array(
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ),
    load: false,
    login: {
      email: "",
      password: ""
    },
    singup: {
      email: "",
      password: ""
    },
    user: {},
    temp_profile_picture: {
      name: "No file uploaded",
      url: null,
      file: null
    },
    experience: {},
    experiences: new Array(),
    education: {},
    educations: new Array(),
    project: {},
    projects: new Array(),
    contribution: {},
    contributions: new Array(),
    skill: {
      porsentage: 50
    },
    skills: new Array(),
    lenguage: {
      porsentage: 50
    },
    lenguages: new Array(),
    template: {},
    templates: new Array(),
    modal: {
      name: false,
      profession: false,
      picture: false,
      profile: false,
      contact: false,
      skill: false
    },
    skill_slider: 50
  },
  mounted() {
    if (localStorage.getItem("php-resume-user")) {
      try {
        let userId = JSON.parse(localStorage.getItem("php-resume-user"));
        this.user.id = userId;
        this.getUser();
        this.getExperiences();
        this.getEducations();
        this.getProjects();
        this.getContribution();
        this.getSkill();
        this.getLenguage();
      } catch (e) {
        localStorage.removeItem("php-resume-user");
      }
    }
  },
  computed: {
    pictureProfile: function() {
      if (this.user.photo_link == null) {
        if (this.temp_profile_picture.url == null) {
          return "imgs/profile.png";
        } else {
          return this.temp_profile_picture.url;
        }
      }
      {
        if (this.temp_profile_picture.url == null) {
          return this.user.photo_link;
        } else {
          return this.temp_profile_picture.url;
        }
      }
    }
  },
  methods: {
    // User
    loginUser() {
      let formdata = new FormData();
      formdata.append("email", this.login.email);
      formdata.append("password", this.login.password);
      axios({
        method: "post",
        url: "api/user.php?action=login",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.login.notification = false;

            this.user = response.data[0];
            localStorage.setItem(
              "php-resume-user",
              JSON.stringify(this.user.id)
            );

            window.location.href = "/wizard";
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    getUser() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/user.php?action=get_user",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.user = response.data[0];
            this.user.first_login = false;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    logOut() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/user.php?action=logout",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.updateUser();
              this.user = {};
              localStorage.removeItem("php-resume-user");
              window.location.href = "/";
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    singUpUser() {
      let formdata = new FormData();
      formdata.append("email", this.singup.email);
      formdata.append("password", this.singup.password);
      axios({
        method: "post",
        url: "api/user.php?action=singup",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.login.email = this.singup.email;
              this.login.password = this.singup.password;
              this.loginUser();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    updateUser() {
      let formdata = this.toFormData(this.user);
      if (this.temp_profile_picture.file != null) {
        formdata.append("file", this.temp_profile_picture.file);
      }
      axios({
        method: "post",
        url: "api/user.php?action=update",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.getUser();
            this.closeModal();
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    previewPicture(event) {
      let files = event.target.files;
      if (files.length > 0) {
        var reader = new FileReader();

        reader.onload = function(e) {
          app.temp_profile_picture.url = e.target.result;
        };

        reader.readAsDataURL(files[0]);

        this.temp_profile_picture.name = files[0].name;
        this.temp_profile_picture.file = files[0];
      }
    },
    discardPreviewPicture() {
      this.temp_profile_picture.name = "No file uploaded";
      this.temp_profile_picture.url = null;
      this.temp_profile_picture.file = null;
      this.modal_picture = false;
    },

    // experience
    saveExperience() {
      this.experience.user_id = this.user.id;
      this.dateSelect();
      let formdata = this.toFormData(this.experience);

      if (!this.experience.id) {
        axios({
          method: "post",
          url: "api/experience.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getExperiences();
                this.cleanExperience();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/experience.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getExperiences();
                this.cleanExperience();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getExperiences() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/experience.php?action=get_experiences",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.experiences = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteExperience: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.experiences[index].id);
      axios({
        method: "post",
        url: "api/experience.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getExperiences();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editExperience: function(index) {
      this.experience = this.experiences[index];

      this.experience.startDate =
        this.experience.start_year +
        "-" +
        this.monthNumber(this.experience.start_month);

      this.experience.endDate =
        this.experience.finish_year +
        "-" +
        this.monthNumber(this.experience.finish_month);
    },
    cleanExperience() {
      this.experience = {};
    },
    dateSelect() {
      var res = this.experience.startDate.split("-");
      this.experience.start_year = res[0];
      this.experience.start_month = this.month(res[1]);

      res = this.experience.endDate.split("-");
      this.experience.finish_year = res[0];
      this.experience.finish_month = this.month(res[1]);
    },

    // education
    saveEducation() {
      this.education.user_id = this.user.id;
      this.dateSelectEducation();
      let formdata = this.toFormData(this.education);

      if (!this.education.id) {
        axios({
          method: "post",
          url: "api/education.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getEducations();
                this.cleanEducation();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/education.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getEducations();
                this.cleanEducation();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getEducations() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/education.php?action=get_educations",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.educations = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteEducation: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.educations[index].id);
      axios({
        method: "post",
        url: "api/education.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getEducations();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editEducation: function(index) {
      this.education = this.educations[index];

      this.education.startDate =
        this.education.start_year +
        "-" +
        this.monthNumber(this.education.start_month);

      this.education.endDate =
        this.education.finish_year +
        "-" +
        this.monthNumber(this.education.finish_month);
    },
    cleanEducation() {
      this.education = {};
    },
    dateSelectEducation() {
      var res = this.education.startDate.split("-");
      this.education.start_year = res[0];
      this.education.start_month = this.month(res[1]);

      res = this.education.endDate.split("-");
      this.education.finish_year = res[0];
      this.education.finish_month = this.month(res[1]);
    },

    // projects
    saveProjects() {
      this.project.user_id = this.user.id;
      let formdata = this.toFormData(this.project);

      if (!this.project.id) {
        axios({
          method: "post",
          url: "api/projects.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getProjects();
                this.cleanProjects();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/projects.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getProjects();
                this.cleanProjects();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getProjects() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/projects.php?action=get_projects",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.projects = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteProjects: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.projects[index].id);
      axios({
        method: "post",
        url: "api/projects.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getProjects();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editProjects: function(index) {
      this.project = this.projects[index];
    },
    cleanProjects() {
      this.project = {};
    },

    // contributions
    saveContribution() {
      this.contribution.user_id = this.user.id;
      let formdata = this.toFormData(this.contribution);

      if (!this.contribution.id) {
        axios({
          method: "post",
          url: "api/contribution.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getContribution();
                this.cleanContribution();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/contribution.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getContribution();
                this.cleanContribution();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getContribution() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/contribution.php?action=get_contributions",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.contributions = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteContribution: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.contributions[index].id);
      axios({
        method: "post",
        url: "api/contribution.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getContribution();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editContribution: function(index) {
      this.contribution = this.contributions[index];
    },
    cleanContribution() {
      this.contribution = {};
    },

    // templates
    getTemplates() {
      this.load = true;
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/template.php?action=get_templates",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.templates = response.data;
            this.load = false;
          } else if (response.data["notification"]) {
            this.load = false;
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },

    // skills
    saveSkill() {
      this.skill.user_id = this.user.id;
      let formdata = this.toFormData(this.skill);

      if (!this.skill.id) {
        axios({
          method: "post",
          url: "api/skill.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getSkill();
                this.cleanSkill();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/skill.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getSkill();
                this.cleanSkill();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getSkill() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/skill.php?action=get_skills",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.skills = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteSkill: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.skills[index].id);
      axios({
        method: "post",
        url: "api/skill.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getSkill();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editSkill: function(index) {
      this.skill = this.skills[index];
    },
    cleanSkill() {
      this.skill = { porsentage: 50 };
    },

    // lenguages
    saveLenguage() {
      this.lenguage.user_id = this.user.id;
      let formdata = this.toFormData(this.lenguage);

      if (!this.lenguage.id) {
        axios({
          method: "post",
          url: "api/lenguage.php?action=insert",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getLenguage();
                this.cleanLenguage();
                this.closeModal();
              }

              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        axios({
          method: "post",
          url: "api/lenguage.php?action=update",
          data: formdata,
          config: { headers: { "Content-Type": "multipart/form-data" } }
        })
          .then(response => {
            if (response.data["notification"]) {
              if (response.data["type"] == "is-success") {
                this.getLenguage();
                this.cleanLenguage();
                this.closeModal();
              }
              bulmaToast.toast({
                message: response.data["message"],
                type: response.data["type"],
                pauseOnHover: true
              });
            }
          })
          .catch(error => {
            console.log(error);
          });
      }
    },
    getLenguage() {
      let formdata = new FormData();
      formdata.append("id", this.user.id);
      axios({
        method: "post",
        url: "api/lenguage.php?action=get_lenguages",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (!response.data["notification"]) {
            this.lenguages = response.data;
          } else if (response.data["notification"]) {
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    deleteLenguage: function(index) {
      let formdata = new FormData();
      formdata.append("id", this.lenguages[index].id);
      axios({
        method: "post",
        url: "api/lenguage.php?action=delete",
        data: formdata,
        config: { headers: { "Content-Type": "multipart/form-data" } }
      })
        .then(response => {
          if (response.data["notification"]) {
            if (response.data["type"] == "is-success") {
              this.getLenguage();
            }
            bulmaToast.toast({
              message: response.data["message"],
              type: response.data["type"],
              pauseOnHover: true
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    },
    editLenguage: function(index) {
      this.lenguage = this.lenguages[index];
    },
    cleanLenguage() {
      this.lenguage = { porsentage: 50 };
    },

    // helpers
    month(month_number) {
      return this.months[month_number - 1];
    },
    monthNumber(month) {
      let x = 0;
      for (let i = 0; i < this.months.length; i++) {
        if (this.months[i] == month) {
          x = i + 1;
          if (x < 10) {
            return "0" + x;
          }
          return x;
        }
      }
    },
    showPassword() {
      if (this.$refs.password.type === "password") {
        this.$refs.password.type = "text";
      } else {
        this.$refs.password.type = "password";
      }
    },
    showPasswordSingup() {
      if (this.$refs.password_singup.type === "password") {
        this.$refs.password_singup.type = "text";
      } else {
        this.$refs.password_singup.type = "password";
      }
    },
    sliderValue() {
      console.log(this.value);
    },
    toFormData(obj) {
      var form_data = new FormData();
      for (var key in obj) {
        form_data.append(key, obj[key]);
      }
      return form_data;
    },
    closeModal() {
      for (var key in this.modal) {
        this.modal[key] = false;
      }
    },
    nowTag(type) {
      switch (type) {
        case "experience":
          return this.experiences[0].finish_year;
        case "education":
          return this.educations[0].finish_year;
      }
    },
    pastTag(type) {
      switch (type) {
        case "experience":
          return this.experiences[this.experiences.length - 1].start_year;
        case "education":
          return this.educations[this.educations.length - 1].start_year;
      }
    }
  }
});

// menu logic
document.addEventListener("DOMContentLoaded", () => {
  // Get all "navbar-burger" elements
  const $navbarBurgers = Array.prototype.slice.call(
    document.querySelectorAll(".navbar-burger"),
    0
  );

  // Check if there are any navbar burgers
  if ($navbarBurgers.length > 0) {
    // Add a click event on each of them
    $navbarBurgers.forEach(el => {
      el.addEventListener("click", () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle("is-active");
        $target.classList.toggle("is-active");
      });
    });
  }
});
