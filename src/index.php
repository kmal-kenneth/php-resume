<?php
require_once './shared/header.php';
require_once './shared/db.php';
require_once './shared/sessions.php';

if (isset($_SESSION['user_id'])) {
    return header('Location: /wizard');
}
?>


<div class="columns is-centered">
    <div class="column is-half">
        <div class="box">
            <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="email" name="email" placeholder="Email" v-model="login.email"><span
                        class="icon is-small is-left"><i class="fas fa-envelope"></i></span>
                </p>
            </div>
            <div class="field has-addons">
                <p class="control has-icons-left is-expanded">
                    <input class="input" type="password" name="password" placeholder="Password" v-model="login.password"
                        ref="password"><span class="icon is-small is-left"><i class="fas fa-lock"></i></span>
                </p>
                <p class="control"><a class="button is-info" @click="showPassword"><i class="fas fa-eye"></i></a>
                </p>
            </div>
            <div class="field is-grouped is-grouped-right">
                <p class="control">
                    <button class="button is-success" @click="loginUser">Log in</button>
                </p>
            </div>
        </div>
    </div>
    <div class="column is-half">
        <div class="box">
            <div class="field">
                <p class="control has-icons-left">
                    <input class="input" type="email" name="email" placeholder="Email" v-model="singup.email"><span
                        class="icon is-small is-left"><i class="fas fa-envelope"></i></span>
                </p>
            </div>
            <div class="field has-addons">
                <p class="control has-icons-left is-expanded">
                    <input class="input" type="password" name="password" placeholder="Password"
                        v-model="singup.password" ref="password_singup"><span class="icon is-small is-left"><i
                            class="fas fa-lock"></i></span>
                </p>
                <p class="control"><a class="button is-info" @click="showPasswordSingup"><i class="fas fa-eye"></i></a>
                </p>
            </div>
            <div class="field is-grouped is-grouped-right">
                <p class="control">
                    <button class="button is-success" @click="singUpUser">Sign in</button>
                </p>
            </div>
        </div>
    </div>
</div>

<?php require_once './shared/footer.php'; ?>
