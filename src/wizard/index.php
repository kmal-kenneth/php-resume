<?php
require_once '../shared/header.php';
require_once '../shared/guard.php';
?>

<div class="columns is-centered">
    <div class="column ">
        <div class="box">
            <div class="steps" id="stepsDemo">
                <div class="step-item is-active is-success">
                    <div class="step-marker">1</div>
                    <div class="step-details">
                        <p class="step-title is-size-6">Personal information</p>
                    </div>
                </div>
                <div class="step-item">
                    <div class="step-marker">2</div>
                    <div class="step-details">
                        <p class="step-title is-size-6">Photography</p>
                    </div>
                </div>
                <div class="step-item">
                    <div class="step-marker">4</div>
                    <div class="step-details">
                        <p class="step-title is-size-6">Experience & Education</p>
                    </div>
                </div>
                <div class="step-item">
                    <div class="step-marker">6</div>
                    <div class="step-details">
                        <p class="step-title is-size-6">Skills & Languages</p>
                    </div>
                </div>
                <div class="step-item">
                    <div class="step-marker">7</div>
                    <div class="step-details">
                        <p class="step-title is-size-6">Projects & Contributions</p>
                    </div>
                </div>
                <div class="steps-content">

                    <!-- Personal information -->
                    <div class="step-content is-active">
                        <?php require_once '../form/personalInfo.php'; ?>
                    </div>

                    <!-- Picture step -->
                    <div class="step-content">
                        <?php require_once '../form/photography.php'; ?>
                    </div>

                    <!-- Experience step -->
                    <div class="step-content">
                        <?php require_once '../form/experience&education.php'; ?>
                    </div>
                    <!-- Knowledge step -->
                    <div class="step-content">
                        <?php require_once '../form/skills&languages.php'; ?>
                    </div>
                    <!-- Projects step -->
                    <div class="step-content">
                        <?php require_once '../form/projects&contrib.php'; ?>
                    </div>
                </div>
                <div class="steps-actions">
                    <div class="steps-action"><a class="button is-light" href="#" data-nav="previous">Previous</a></div>
                    <div class="steps-action"><a class="button is-light" href="#" data-nav="next">Next</a></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once '../shared/footer.php'; ?>
