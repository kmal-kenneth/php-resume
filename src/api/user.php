<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'login':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $results = $userModel->login($email, $password);

            if ($results) {
                $responce = $results;
                $_SESSION['user_id'] = $results[0]['id'];
                $_SESSION['user_email'] = $results[0]['email'];
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Invalid email or password.';
            }
        }
        break;
    case 'logout':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            if (isset($_SESSION['user_id'])) {
                if ($_SESSION['user_id'] == $id) {
                    // remove all session variables
                    session_unset();
                    // destroy the session
                    session_destroy();

                    $notification = true;
                    $type = "is-success";
                    $message = 'Log out.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Wrong id.';
                }
            } else {
                $notification = true;
                $type = "is-info";
                $message = 'You must log in first.';
            }
        }
        break;
    case 'get_user':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $userModel->find($id);

            if ($results) {
                $responce = $results;
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'User not found.';
            }
        }
        break;
    case 'singup':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['email'] != null && $_POST['password'] != null) {
                $email = $_POST['email'];
                $password = $_POST['password'];

                $results = $userModel->create($email, $password);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'User insert.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Could not create account. Try another email.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Email and password are required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];
            $email = $_POST['email'];
            $first_login = $_POST['first_login'];
            $first_name = $_POST['first_name'];
            $middle_name = $_POST['middle_name'];
            $last_name = $_POST['last_name'];
            $profession = $_POST['profession'];
            $photo_link = $_POST['photo_link'];
            $profile = $_POST['profile'];
            $phone_number = $_POST['phone_number'];
            $web = $_POST['web'];
            $git_repository = $_POST['git_repository'];
            $address = $_POST['address'];
            $city = $_POST['city'];
            $country = $_POST['country'];
            $knowledge = $_POST['knowledge'];
            $year_of_birth = $_POST['year_of_birth'];
            $place_of_birth = $_POST['place_of_birth'];
            $citizen_id = $_POST['citizen_id'];
            $driver_license = $_POST['driver_license'];

            if (isset($_FILES['file']['name'])) {
                $file = $_FILES['file']['name'];
                $target_dir = "../imgs/users/";
                $target_file =
                    $target_dir . "id:" . $id . "-" . basename($file);
                move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
                if (
                    $photo_link != "../imgs/profile.png" &&
                    $photo_link != "null"
                ) {
                    unlink($photo_link);
                }
                $photo_link = $target_file;
            }

            $results = $userModel->update(
                $id,
                $email,
                $first_login,
                $first_name,
                $middle_name,
                $last_name,
                $profession,
                $photo_link,
                $profile,
                $phone_number,
                $web,
                $git_repository,
                $address,
                $city,
                $country,
                $knowledge,
                $year_of_birth,
                $place_of_birth,
                $citizen_id,
                $driver_license
            );

            if ($results) {
                $notification = true;
                $type = "is-success";
                $message = 'User updated.';
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Could not update user.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
