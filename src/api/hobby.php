<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['hobby'] != null &&
                $_POST['icon'] != null
            ) {
                $user_id = $_POST['user_id'];
                $hobby = $_POST['hobby'];
                $icon = $_POST['icon'];
                $web = $_POST['web'];

                $results = $hobbyModel->insert(
                    $user_id,
                    $hobby,
                    $icon,
                    $web
                );
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Hobby created successfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to create hobby...';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Hobby name is required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['hobby'] != null &&
                $_POST['icon'] != null
            ) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $hobby = $_POST['hobby'];
                $icon = $_POST['icon'];
                $web = $_POST['web'];

                $results = $hobbyModel->update(
                    $id,
                    $user_id,
                    $hobby,
                    $icon,
                    $web
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Hobby updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update hobby.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Hobby name is required.';
            }
        }
        break;
    case 'get_hobbies':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $hobbyModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $hobbyModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Hobby deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete hobby.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires hobby id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>

