<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['lenguage'] != null
            ) {
                $user_id = $_POST['user_id'];
                $lenguage = $_POST['lenguage'];
                $porsentage = $_POST['porsentage'];

                $results = $lenguageModel->insert(
                    $user_id,
                    $lenguage,
                    $porsentage
                );
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Language created successfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to create language...';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Language name is required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['lenguage'] != null
            ) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $lenguage = $_POST['lenguage'];
                $porsentage = $_POST['porsentage'];

                $results = $lenguageModel->update(
                    $id,
                    $user_id,
                    $lenguage,
                    $porsentage
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Language updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update language.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Language name is required.';
            }
        }
        break;
    case 'get_lenguages':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $lenguageModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $lenguageModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Language deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete Language.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires language id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
