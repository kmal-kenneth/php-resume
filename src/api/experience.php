<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['start_year'] != null &&
                $_POST['finish_year'] != null &&
                $_POST['job_description'] != null
            ) {
                $user_id = $_POST['user_id'];
                $start_year = $_POST['start_year'];
                $start_month = $_POST['start_month'];
                $finish_year = $_POST['finish_year'];
                $finish_month = $_POST['finish_month'];
                $position = $_POST['position'];
                $company_name = $_POST['company_name'];
                $company_web = $_POST['company_web'];
                $job_description = $_POST['job_description'];

                $results = $experienceModel->insert(
                    $user_id,
                    $start_year,
                    $start_month,
                    $finish_year,
                    $finish_month,
                    $position,
                    $company_name,
                    $company_web,
                    $job_description
                );
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Experience created succesfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to insert experience...';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Start year, finish year and job description are required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['start_year'] != null &&
                $_POST['finish_year'] != null &&
                $_POST['job_description'] != null
            ) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $start_year = $_POST['start_year'];
                $start_month = $_POST['start_month'];
                $finish_year = $_POST['finish_year'];
                $finish_month = $_POST['finish_month'];
                $position = $_POST['position'];
                $company_name = $_POST['company_name'];
                $company_web = $_POST['company_web'];
                $job_description = $_POST['job_description'];

                $results = $experienceModel->update(
                    $id,
                    $user_id,
                    $start_year,
                    $start_month,
                    $finish_year,
                    $finish_month,
                    $position,
                    $company_name,
                    $company_web,
                    $job_description
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Experience updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update experience.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Start year, finish year and job description are required.';
            }
        }
        break;
    case 'get_experiences':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $experienceModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $experienceModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Experience deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete experience.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires experience id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
