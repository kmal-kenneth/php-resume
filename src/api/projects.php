<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['user_id'] != null && $_POST['name'] != null) {
                $user_id = $_POST['user_id'];
                $name = $_POST['name'];
                $web = $_POST['web'];
                $description = $_POST['description'];
                $platform = $_POST['platform'];

                $results = $projectModel->insert(
                    $user_id,
                    $name,
                    $web,
                    $description,
                    $platform
                );
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Project created successfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to create project.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Project name is required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['user_id'] != null && $_POST['name'] != null) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $name = $_POST['name'];
                $web = $_POST['web'];
                $description = $_POST['description'];
                $platform = $_POST['platform'];

                $results = $projectModel->update(
                    $id,
                    $user_id,
                    $name,
                    $web,
                    $description,
                    $platform
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Project updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update project.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Project name is required.';
            }
        }
        break;
    case 'get_projects':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $projectModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $projectModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Project deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete project.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires project id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
