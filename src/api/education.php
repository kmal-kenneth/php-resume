<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['start_year'] != null &&
                $_POST['finish_year'] != null &&
                $_POST['degree'] != null
            ) {
                $user_id = $_POST['user_id'];
                $start_year = $_POST['start_year'];
                $start_month = $_POST['start_month'];
                $finish_year = $_POST['finish_year'];
                $finish_month = $_POST['finish_month'];
                $degree = $_POST['degree'];
                $institution_name = $_POST['institution_name'];
                $institution_web = $_POST['institution_web'];
                $degree_description = $_POST['degree_description'];

                $results = $educationModel->insert(
                    $user_id,
                    $start_year,
                    $start_month,
                    $finish_year,
                    $finish_month,
                    $degree,
                    $institution_name,
                    $institution_web,
                    $degree_description
                );
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Education created successfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to create education...';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Start year, finish year and the degree are required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (
                $_POST['user_id'] != null &&
                $_POST['start_year'] != null &&
                $_POST['finish_year'] != null &&
                $_POST['degree'] != null
            ) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $start_year = $_POST['start_year'];
                $start_month = $_POST['start_month'];
                $finish_year = $_POST['finish_year'];
                $finish_month = $_POST['finish_month'];
                $degree = $_POST['degree'];
                $institution_name = $_POST['institution_name'];
                $institution_web = $_POST['institution_web'];
                $degree_description = $_POST['degree_description'];

                $results = $educationModel->update(
                    $id,
                    $user_id,
                    $start_year,
                    $start_month,
                    $finish_year,
                    $finish_month,
                    $degree,
                    $institution_name,
                    $institution_web,
                    $degree_description
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Education updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update education.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Start year, finish year and degree are required.';
            }
        }
        break;
    case 'get_educations':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $educationModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $educationModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Education deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete education.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires education id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
