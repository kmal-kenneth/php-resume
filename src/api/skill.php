<?php
require_once '../shared/db.php';
require_once '../shared/sessions.php';

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

switch ($action) {
    case 'insert':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['user_id'] != null && $_POST['skill'] != null) {
                $user_id = $_POST['user_id'];
                $skill = $_POST['skill'];
                $porsentage = $_POST['porsentage'];

                $results = $skillModel->insert($user_id, $skill, $porsentage);
                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = "Skill created successfully.";
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to create skill...';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Skill name is required.';
            }
        }
        break;
    case 'update':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['user_id'] != null && $_POST['skill'] != null) {
                $id = $_POST['id'];
                $user_id = $_POST['user_id'];
                $skill = $_POST['skill'];
                $porsentage = $_POST['porsentage'];

                $results = $skillModel->update(
                    $id,
                    $user_id,
                    $skill,
                    $porsentage
                );

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Skill updated successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to update skill.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Skill name is required.';
            }
        }
        break;
    case 'get_skills':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $results = $skillModel->getAll($id);

            if ($results) {
                $responce = $results;
            }
        }
        break;
    case 'delete':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['id'] != null) {
                $id = $_POST['id'];

                $results = $skillModel->delete($id);

                if ($results) {
                    $notification = true;
                    $type = "is-success";
                    $message = 'Skill deleted successfully.';
                } else {
                    $notification = true;
                    $type = "is-danger";
                    $message = 'Failed to delete Skill.';
                }
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Requires skill id.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
