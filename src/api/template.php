<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Nesk\Puphpeteer\Puppeteer;

$puppeteer = new Puppeteer();

// action to make by get
$action = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

// response to return
$responce = array();
$notification = false;
$type = "";
$message = '';

// $target_dir = "../imgs/users/";
//         $target_file = $target_dir . "id:" . $id . "-" . basename($file);
//         move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

switch ($action) {
    case 'get_templates':
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $id = $_POST['id'];

            $templates = glob('../template/templates/*/index.html');

            foreach ($templates as $key => $value) {
                $html = __DIR__ . "/" . $value;

                $preName = str_replace(
                    __DIR__ . "/../template/templates/",
                    "",
                    $html
                );
                $name =
                    'id=' .
                    $id .
                    '-' .
                    str_replace("/index.html", "", $preName);

                $browser = $puppeteer->launch();

                $page = $browser->newPage();
                $page->goto('file://' . $html, [
                    'waitUntil' => 'networkidle0'
                ]);
                $page->screenshot([
                    'path' => '../imgs/template/' . $name . '.png',
                    'fullPage' => true
                ]);

                $browser->close();

                $browser = $puppeteer->launch();

                $page = $browser->newPage();
                $page->goto('file://' . $html, [
                    'waitUntil' => 'networkidle0'
                ]);
                $page->pdf([
                    'path' => '../pdf/' . $name . '.pdf',
                    'format' => 'Letter',
                    'printBackground' => true
                ]);

                $browser->close();
            }

            $results = array();

            $templatesImage = glob('../imgs/template/id=' . $id . '-*.png');
            foreach ($templatesImage as $key => $value) {
                $preName = str_replace("../imgs/template/", "", $value);
                $name = str_replace(".png", "", $preName);

                $results[$key]->picture = false;
                $results[$key]->img = $value;
                $results[$key]->pdf = '../pdf/' . $name . '.pdf';
            }

            if ($results) {
                $responce = $results;
            } else {
                $notification = true;
                $type = "is-danger";
                $message = 'Template not found.';
            }
        }
        break;
}

if ($notification) {
    $responce[notification] = $notification;
    $responce[type] = $type;
    $responce[message] = $message;
}

header('content-type: application/json');
echo json_encode($responce);
die();

?>
