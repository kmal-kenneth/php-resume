<div class="field has-border--bottom">
    <label class="label">Contact information</label>
</div>

<div class="field">
    <label class="label">Phone Number</label>
    <div class="control is-expanded has-icons-left">
        <input class="input is-small is-rounded" type="text" placeholder="Where companies can get you"
            v-model="user.phone_number">
        <span class="icon is-small is-left">
            <i class="fas fa-mobile"></i>
        </span>
    </div>
</div>

<div class="field">
    <label class="label">E-mail</label>
    <div class="control is-expanded has-icons-left">
        <input class="input is-small is-rounded" type="text" placeholder="Try to keep it seriuos" v-model="user.email">
        <span class="icon is-small is-left">
            <i class="fas fa-at"></i>
        </span>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Web presence</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Web page" v-model="user.web">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="GIT User"
                    v-model="user.git_repository">
            </p>
        </div>
    </div>
</div>


<div class="field is-horizontal">
    <div class="field-label">
        <!-- Left empty for spacing -->
    </div>
    <div class="field-body">
        <div class="field is-grouped is-grouped-right">
            <div class="control">
                <button class="button is-primary is-rounded is-small" @click="updateUser">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>