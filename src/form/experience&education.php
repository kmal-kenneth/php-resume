<div class="columns footerresume is-variable is-4">
    <div class="column">
        <div class="has-padding--bottom has-border--bottom has-margin--bottom">
            <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">EXPERIENCE</h3>


            <div class="field has-border--bottom"><label class="label">Dates</label></div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Start</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control is-expanded">
                            <input class="input is-small is-rounded select-month" type="month" id="bdaymonth"
                                name="bdaymonth" v-model="experience.startDate">
                        </div>
                    </div>
                </div>
                <div class="field-label">
                    <!-- Left empty for spacing -->
                </div>
                <div class="field-label is-normal">
                    <label class="label">Finish</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control is-expanded">
                            <input class="input is-small is-rounded select-month" type="month" id="bdaymonth"
                                name="bdaymonth" v-model="experience.endDate">
                        </div>
                    </div>
                </div>
            </div>

            <div class="field has-border--bottom"><label class="label">Position</label></div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded "><input type="text" placeholder="Your role in the company"
                                class="input is-rounded is-small" v-model="experience.position"></p>
                    </div>
                </div>
            </div>
            <div class="field has-border--bottom"><label class="label">Company information</label></div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded"><input type="text" placeholder="Name"
                                class="input is-rounded is-small" v-model="experience.company_name">
                        </p>
                    </div>
                    <div class="field">
                        <p class="control is-expanded"><input type="text" placeholder="Web"
                                class="input is-rounded is-small" v-model="experience.company_web">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field has-border--bottom"><label class="label">Job description</label></div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded "><textarea placeholder="What you used to do in the company"
                                rows="4" cols="50" class="textarea has-fixed-size is-small"
                                v-model="experience.job_description"></textarea></p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label">
                    <!-- Left empty for spacing -->
                </div>
                <div class="field-body">
                    <div class="field is-grouped is-grouped-right">
                        <div class="control">
                            <button class="button is-danger is-rounded is-small" @click="cleanExperience">
                                Discard
                            </button>
                        </div>
                        <div class="control">
                            <button class="button is-primary is-rounded is-small" @click="saveExperience">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="timeline" v-if="experiences.length">
            <header class="timeline-header"><span class="tag is-medium-isprimary">{{nowTag('experience')}}</span>
            </header>

            <!-- Timeline item -->
            <div class="timeline-item" v-for="(item, index) in experiences" :key="item.id">
                <div class="timeline-marker"></div>
                <div class="timeline-content timeline-argument">
                    <div class="header-section">
                        <div class="header-section-left">
                            <p class="timeline-argument-timeperiod">
                                {{item.start_month}} {{item.start_year}} - {{item.finish_month}} {{item.finish_year}}
                            </p>
                        </div>
                        <div class="header-section-rigth">
                            <span class="icon iconbutton" @click="editExperience(index)">
                                <i class="fas fa-pen"></i>
                            </span>
                            <span class="icon iconbutton" @click="deleteExperience(index)">
                                <i class="fas fa-trash"></i>
                            </span>
                        </div>
                    </div>
                    <h4 class="timeline-argument-position">
                        {{item.position}}</h4>
                    <h5 class="timeline-argument-company">{{item.company_name}} - <a class="timeline-argument-website">
                            {{item.company_web}}</a></h5>
                    <p class="timeline-argument-description">
                        {{item.job_description}}</p>
                </div>
            </div>

            <header class="timeline-header"><span class="tag is-medium-isprimary">{{pastTag('experience')}}</span>
            </header>
        </div>
    </div>
    <div class="column">

        <div class="has-padding--bottom has-border--bottom has-margin--bottom">
            <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">EDUCATION</h3>


            <div class="field has-border--bottom"><label class="label">Dates</label></div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Start</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control is-expanded">
                            <input class="input is-small is-rounded select-month" type="month" id="bdaymonth"
                                name="bdaymonth" v-model="education.startDate">
                        </div>
                    </div>
                </div>
                <div class="field-label">
                    <!-- Left empty for spacing -->
                </div>
                <div class="field-label is-normal">
                    <label class="label">Finish</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <div class="control is-expanded">
                            <input class="input is-small is-rounded select-month" type="month" id="bdaymonth"
                                name="bdaymonth" v-model="education.endDate">
                        </div>
                    </div>
                </div>
            </div>

            <div class="field has-border--bottom"><label class="label">Degree</label></div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded "><input type="text" placeholder="The title you obtained"
                                class="input is-rounded is-small" v-model="education.degree"></p>
                    </div>
                </div>
            </div>
            <div class="field has-border--bottom"><label class="label">Educational institution</label></div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded"><input type="text" placeholder="Name of the center"
                                class="input is-rounded is-small" v-model="education.institution_name">
                        </p>
                    </div>
                    <div class="field">
                        <p class="control is-expanded"><input type="text" placeholder="Institution website"
                                class="input is-rounded is-small" v-model="education.institution_web">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field has-border--bottom"><label class="label">Degree description</label>
            </div>
            <div class="field is-horizontal">
                <div class="field-body">
                    <div class="field">
                        <p class="control is-expanded "><textarea placeholder="What did you learn to do?" rows="4"
                                cols="50" class="textarea has-fixed-size is-small"
                                v-model="education.degree_description"></textarea>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label">
                    <!-- Left empty for spacing -->
                </div>
                <div class="field-body">
                    <div class="field is-grouped is-grouped-right">
                        <div class="control">
                            <button class="button is-danger is-rounded is-small" @click="cleanEducation">
                                Discard
                            </button>
                        </div>
                        <div class="control">
                            <button class="button is-primary is-rounded is-small" @click="saveEducation">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="timeline" v-if="educations.length">
            <header class="timeline-header"><span class="tag is-medium-isprimary">{{nowTag('education')}}</span>
            </header>

            <!-- Timeline item -->
            <div class="timeline-item" v-for="(item, index) in educations" :key="item.id">
                <div class="timeline-marker"></div>
                <div class="timeline-content timeline-argument">
                    <div class="header-section">
                        <div class="header-section-left">
                            <p class="timeline-argument-timeperiod">
                                {{item.start_month}} {{item.start_year}} - {{item.finish_month}} {{item.finish_year}}
                            </p>
                        </div>
                        <div class="header-section-rigth">
                            <span class="icon iconbutton" @click="editEducation(index)">
                                <i class="fas fa-pen"></i>
                            </span>
                            <span class="icon iconbutton" @click="deleteEducation(index)">
                                <i class="fas fa-trash"></i>
                            </span>
                        </div>
                    </div>
                    <h4 class="timeline-argument-degree">
                        {{item.degree}}</h4>
                    <h5 class="timeline-argument-company">{{item.institution_name}} - <a
                            class="timeline-argument-website">
                            {{item.institution_web}}</a></h5>
                    <p class="timeline-argument-description">
                        {{item.degree_description}}</p>
                </div>
            </div>

            <header class="timeline-header"><span class="tag is-medium-isprimary">{{pastTag('education')}}</span>
            </header>
        </div>
    </div>

</div>