<section>
    <div class="columns is-variable is-8">
        <div class="column">

            <div class="has-padding--bottom has-border--bottom has-margin--bottom">
                <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">Skills</h3>

                <div class="field header-section skill">
                    <div class="header-section-left">
                        <input class="input is-small is-rounded" type="text" placeholder="Skill" value=""
                            v-model="skill.skill">
                    </div>
                    <div class="header-section-rigth header-section-rigth--vcenter">
                        <input id="sliderWithValue" class="slider has-output is-small" min="0" max="100" step="1"
                            type="range" v-model="skill.porsentage" placeholder="sdsdsd">
                        <output class="slideroutput" for="sliderWithValue">{{ skill.porsentage }}</output>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label">
                        <!-- Left empty for spacing -->
                    </div>
                    <div class="field-body">
                        <div class="field is-grouped is-grouped-right">
                            <div class="control">
                                <button class="button is-danger is-rounded is-small" @click="cleanSkill">
                                    Discard
                                </button>
                            </div>
                            <div class="control">
                                <button class="button is-primary is-rounded is-small" @click="saveSkill">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <ul>
                <li class="header-section skill" v-if="skills.length" v-for="(item, index) in skills" :key="item.id">
                    <div class="header-section-left">
                        <p>{{ item.skill }}</p>
                    </div>
                    <div class="header-section-rigth header-section-rigth--vcenter">
                        <progress class="progress is-small" :value="item.porsentage"
                            max="100">{{ item.porsentage }}</progress>
                        <span class="icon iconbutton" @click="editSkill(index)">
                            <i class="fas fa-pen">
                            </i>
                        </span>
                        <span class="icon iconbutton" @click="deleteSkill(index)">
                            <i class="fas fa-trash"></i>
                        </span>
                    </div>
                </li>

            </ul>

        </div>
        <div class="column">




            <div class="has-padding--bottom has-border--bottom has-margin--bottom">
                <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">Lenguages</h3>

                <div class="field header-section lenguage">
                    <div class="header-section-left">
                        <input class="input is-small is-rounded" type="text" placeholder="Lenguage" value=""
                            v-model="lenguage.lenguage">
                    </div>
                    <div class="header-section-rigth header-section-rigth--vcenter">
                        <input id="sliderWithValue" class="slider has-output is-small" min="0" max="100" step="1"
                            type="range" v-model="lenguage.porsentage" placeholder="sdsdsd">
                        <output class="slideroutput" for="sliderWithValue">{{ lenguage.porsentage }}</output>
                    </div>
                </div>

                <div class="field is-horizontal">
                    <div class="field-label">
                        <!-- Left empty for spacing -->
                    </div>
                    <div class="field-body">
                        <div class="field is-grouped is-grouped-right">
                            <div class="control">
                                <button class="button is-danger is-rounded is-small" @click="cleanLenguage">
                                    Discard
                                </button>
                            </div>
                            <div class="control">
                                <button class="button is-primary is-rounded is-small" @click="saveLenguage">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <ul>
                <li class="header-section skill" v-if="lenguages.length" v-for="(item, index) in lenguages"
                    :key="item.id">
                    <div class="header-section-left">
                        <p>{{ item.lenguage }}</p>
                    </div>
                    <div class="header-section-rigth header-section-rigth--vcenter">
                        <progress class="progress is-small" :value="item.porsentage"
                            max="100">{{ item.porsentage }}</progress>
                        <span class="icon iconbutton" @click="editLenguage(index)">
                            <i class="fas fa-pen">
                            </i>
                        </span>
                        <span class="icon iconbutton" @click="deleteLenguage(index)">
                            <i class="fas fa-trash"></i>
                        </span>
                    </div>
                </li>

            </ul>


        </div>
</section>