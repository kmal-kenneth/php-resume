<section>
    <div class="columns footerresume is-variable is-4">
        <div class="column">

            <div class="has-padding--bottom has-border--bottom has-margin--bottom">
                <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">projects</h3>

                <div class="field has-border--bottom"><label class="label">Name</label></div>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded "><input type="text" placeholder="Name"
                                    class="input is-rounded is-small" v-model="project.name"></p>
                        </div>
                    </div>
                </div>
                <div class="field has-border--bottom"><label class="label">General information</label></div>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded"><input type="text" placeholder="Platform"
                                    class="input is-rounded is-small" v-model="project.platform">
                            </p>
                        </div>
                        <div class="field">
                            <p class="control is-expanded"><input type="text" placeholder="Website"
                                    class="input is-rounded is-small" v-model="project.web">
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field has-border--bottom"><label class="label">Description</label>
                </div>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded "><textarea placeholder="What did you do in the project?"
                                    rows="4" cols="50" class="textarea has-fixed-size is-small"
                                    v-model="project.description"></textarea>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label">
                        <!-- Left empty for spacing -->
                    </div>
                    <div class="field-body">
                        <div class="field is-grouped is-grouped-right">
                            <div class="control">
                                <button class="button is-danger is-rounded is-small" @click="cleanProjects">
                                    Discard
                                </button>
                            </div>
                            <div class="control">
                                <button class="button is-primary is-rounded is-small" @click="saveProjects">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="projects.length" v-for="(item, index) in projects" :key="item.id">

                <div class="timeline-content timeline-argument">
                    <div class="header-section">
                        <div class="header-section-left">
                            <h4 class="timeline-argument-position">
                                {{ item.name }} </h4>
                        </div>
                        <div class="header-section-rigth">
                            <span class="icon iconbutton" @click="editProjects(index)">
                                <i class="fas fa-pen"></i>
                            </span>
                            <span class="icon iconbutton" @click="deleteProjects(index)">
                                <i class="fas fa-trash"></i>
                            </span>
                        </div>
                    </div><a class="timeline-argument-website">{{ item.web }}</a>
                    <p class="timeline-argument-description"> {{ item.description }}
                    </p>
                    <h5 class="timeline-argument-company">{{ item.platform }}</h5>
                </div>

            </div>

        </div>
        <div class="column">

            <div class="has-padding--bottom has-border--bottom has-margin--bottom">
                <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">contributions</h3>


                <div class="field has-border--bottom"><label class="label">Name</label></div>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded "><input type="text" placeholder="Name"
                                    class="input is-rounded is-small" v-model="contribution.name"></p>
                        </div>
                    </div>
                </div>
                <div class="field has-border--bottom"><label class="label">Web</label></div>
                <div class="field is-horizontal">
                    <div class="field-body">

                        <div class="field">
                            <p class="control is-expanded"><input type="text" placeholder="Website"
                                    class="input is-rounded is-small" v-model="contribution.web">
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field has-border--bottom"><label class="label">Description</label>
                </div>
                <div class="field is-horizontal">
                    <div class="field-body">
                        <div class="field">
                            <p class="control is-expanded "><textarea placeholder="What did you do in the project?"
                                    rows="4" cols="50" class="textarea has-fixed-size is-small"
                                    v-model="contribution.description"></textarea>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="field is-horizontal">
                    <div class="field-label">
                        <!-- Left empty for spacing -->
                    </div>
                    <div class="field-body">
                        <div class="field is-grouped is-grouped-right">
                            <div class="control">
                                <button class="button is-danger is-rounded is-small" @click="cleanContribution">
                                    Discard
                                </button>
                            </div>
                            <div class="control">
                                <button class="button is-primary is-rounded is-small" @click="saveContribution">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div v-if="projects.length" v-for="(item, index) in contributions" :key="item.id">

                <div class="timeline-content timeline-argument">
                    <div class="header-section">
                        <div class="header-section-left">
                            <h4 class="timeline-argument-position">
                                {{ item.name }}</h4>
                        </div>
                        <div class="header-section-rigth">
                            <span class="icon iconbutton" @click="editContribution(index)">
                                <i class="fas fa-pen"></i>
                            </span>
                            <span class="icon iconbutton" @click="deleteContribution(index)">
                                <i class="fas fa-trash"></i>
                            </span>
                        </div>
                    </div>
                    <a class="timeline-argument-website">
                        {{ item.web }}</a>
                    <p class="timeline-argument-description">
                        {{ item.description }}</p>
                </div>

            </div>
        </div>
    </div>
</section>