<div class="field has-border--bottom">
    <label class="label">Picture</label>
</div>
<div class="columns is-centered">
    <div class="column is-half">
        <div class="field">
            <p class="image">
                <img :src="pictureProfile" alt="Preview of uploaded picture." class="has-ratio">
            </p>
        </div>
    </div>
</div>
<div class="field">
    <div id="file-js-example" class="file has-name is-fullwidth is-right">
        <label class="file-label">
            <input class="file-input" type="file" name="resume" accept='image/*' @change="previewPicture">
            <span class="file-cta">
                <span class="file-icon">
                    <i class="fas fa-upload"></i>
                </span>
                <span class="file-label">
                    Choose your picture...
                </span>
            </span>
            <span class="file-name">
                {{ temp_profile_picture.name }}
            </span>
        </label>
    </div>
</div>

<div class="field is-horizontal">
    <div class="field-label">
        <!-- Left empty for spacing -->
    </div>
    <div class="field-body">
        <div class="field is-grouped is-grouped-right">
            <div class="control">
                <button class="button is-danger is-rounded is-small" @click="discardPreviewPicture">
                    Discard
                </button>
            </div>
            <div class="control">
                <button class="button is-primary is-rounded is-small" @click="updateUser">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>