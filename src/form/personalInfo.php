<div class="field has-border--bottom">
    <label class="label">Name</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="First name" v-model="user.first_name">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Middle name"
                    v-model="user.middle_name">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Last name" v-model="user.last_name">
            </p>
        </div>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Information</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="ID" v-model="user.citizen_id">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Driver's license"
                    v-model="user.driver_license">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Nacionality"
                    v-model="user.place_of_birth">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Birth year"
                    v-model="user.year_of_birth">
            </p>
        </div>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Contact information</label>
</div>

<div class="field">
    <label class="label">Phone number</label>
    <div class="control is-expanded has-icons-left">
        <input class="input is-small is-rounded" type="text" placeholder="Where companies can contact you"
            v-model="user.phone_number">
        <span class="icon is-small is-left">
            <i class="fas fa-mobile"></i>
        </span>
    </div>
</div>

<div class="field">
    <label class="label">E-mail</label>
    <div class="control is-expanded has-icons-left">
        <input class="input is-small is-rounded" type="text" placeholder="Try to keep it seriuos" v-model="user.email">
        <span class="icon is-small is-left">
            <i class="fas fa-at"></i>
        </span>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Web presence</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Web page" v-model="user.web">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="GIT User"
                    v-model="user.git_repository">
            </p>
        </div>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Current residence</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="Country" v-model="user.country">
            </p>
        </div>
        <div class="field">
            <p class="control is-expanded">
                <input class="input is-rounded is-small" type="text" placeholder="State or province"
                    v-model="user.city">
            </p>
        </div>
    </div>
</div>

<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded ">
                <textarea class="textarea has-fixed-size is-small" placeholder="Other signs" v-model="user.address"
                    rows="4" cols="50"></textarea>
            </p>
        </div>

    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Profession</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded ">
                <input class="input is-rounded is-small" type="text" placeholder="Your occupation founded upon specialized educational training"
                    v-model="user.profession">
            </p>
        </div>

    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Professional profile</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded ">
                <textarea class="textarea has-fixed-size is-small" placeholder="Brief summary of skills, strengths and key experiences"
                    v-model="user.profile" rows="4" cols="50"></textarea>
            </p>
        </div>
    </div>
</div>

<div class="field has-border--bottom">
    <label class="label">Other knowledges</label>
</div>
<div class="field is-horizontal">
    <div class="field-body">
        <div class="field">
            <p class="control is-expanded ">
                <textarea class="textarea has-fixed-size is-small"
                    placeholder="Informal learning, conferences, lectures and others" v-model="user.knowledge" rows="4"
                    cols="50"></textarea>
            </p>
        </div>
    </div>
</div>

<!-- Buttom save -->
<div class="field is-horizontal">
    <div class="field-label">
        <!-- Left empty for spacing -->
    </div>
    <div class="field-body">
        <div class="field is-grouped is-grouped-right">
            <div class="control">
                <button class="button is-primary is-rounded is-small" @click="updateUser">
                    Save
                </button>
            </div>
        </div>
    </div>
</div>