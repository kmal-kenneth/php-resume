<?php
require_once '../shared/header.php';
require_once '../shared/guard.php';
?>

<div class="pageloader" v-bind:class="{ 'is-active': load }"><span class="title">Loader</span></div>

<div class="box is-fullwidth">
    <div class="header-section">
        <div class="header-section-left">
            <h3 class="subtitle is-6 has-text-weight-bold is-uppercase">Templates</h3>
        </div>
        <div class="header-section-rigth">
            <button class="button is-info is-rounded is-small" @click="getTemplates">
                Load Templates
            </button>

        </div>
    </div>
</div>

<div class="columns is-multiline">

    <div class="column is-2" v-if="templates.length" v-for="(item, index) in templates" :key="item.id">
        <!-- modal picture -->
        <div class="modal" v-bind:class="{ 'is-active': item.picture }">
            <div class="modal-background" @click="item.picture = false"></div>
            <div class="modal-content is-full">
                <div class="box">

                    <div class="field has-border--bottom">
                        <label class="label">Template</label>
                    </div>
                    <div class="columns is-centered">
                        <div class="column ">
                            <div class="field">
                                <p class="image">
                                    <img :src="item.img" alt="Tamplate picture." class="has-ratio">
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <button class="modal-close is-large" aria-label="close" @click="item.picture = false"></button>
        </div>
        <div class="card">
            <div class="card-image">
                <figure class="image is-9by16" @click="item.picture = true">
                    <img :src="item.img" alt="Placeholder image" class="has-ratio">
                </figure>

            </div>
            <footer class="card-footer">
                <a href="#" class="card-footer-item">Send</a>
                <a :href="item.pdf" class="card-footer-item">Download</a>
            </footer>
        </div>
    </div>


</div>

<?php require_once '../shared/footer.php'; ?>
