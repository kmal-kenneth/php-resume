CREATE TABLE "user" (
  "id" serial PRIMARY KEY,
  "email" text UNIQUE NOT NULL,
  "password" text NOT NULL,
  "first_login" bool DEFAULT true,
  "first_name" text,
  "middle_name" text,
  "last_name" text,
  "profession" text,
  "photo_link" text,
  "profile" text,
  "phone_number" text,
  "web" text,
  "git_repository" text,
  "address" text,
  "city" text,
  "country" text,
  "knowledge" text,
  "year_of_birth" int,
  "place_of_birth" text,
  "citizen_id" text,
  "driver_license" text
);

CREATE TABLE "experience" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "start_year" int NOT NULL,
  "start_month" text,
  "finish_year" int NOT NULL,
  "finish_month" text,
  "position" text,
  "company_name" text,
  "company_web" text,
  "job_description" text NOT NULL
);

CREATE TABLE "education" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "start_year" int NOT NULL,
  "start_month" text,
  "finish_year" int NOT NULL,
  "finish_month" text,
  "degree" text NOT NULL,
  "institution_name" text,
  "institution_web" text,
  "degree_description" text
);

CREATE TABLE "project" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "name" text NOT NULL,
  "web" text,
  "description" text,
  "platform" text
);

CREATE TABLE "contribution" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "name" text NOT NULL,
  "web" text,
  "description" text
);

CREATE TABLE "skill" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "skill" text NOT NULL,
  "porsentage" int DEFAULT 0
);

CREATE TABLE "lenguage" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "lenguage" text NOT NULL,
  "porsentage" int DEFAULT 0
);

CREATE TABLE "hobby" (
  "id" serial PRIMARY KEY,
  "user_id" serial,
  "hobby" text NOT NULL,
  "icon" text NOT NULL,
  "web" text
);

ALTER TABLE "experience" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "education" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "project" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "contribution" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "skill" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "lenguage" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "hobby" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

